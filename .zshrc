# zoomershellrc

source ~/.config/shell/profile
autoload -U colors && colors	# Load colors
# set $PS1 just incase starship does not load
#PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
#PS1="%~ $ "
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments
#setopt autocd

# make cd work like pushd
setopt auto_pushd

# History in cache directory:
setopt HIST_IGNORE_DUPS
HISTSIZE=20000
SAVEHIST=20000
HISTFILE=~/.cache/zsh/history

# Load aliases and shortcuts if existent.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc.bash" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc.bash"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/funcrc.bash" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/funcrc.bash"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1
bindkey '^R' history-incremental-search-backward

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
# commented out because some programs hate it
#function zle-keymap-select () {
#    case $KEYMAP in
#        vicmd) echo -ne '\e[1 q';;      # block
#        viins|main) echo -ne '\e[5 q';; # beam
#    esac
#}
#zle -N zle-keymap-select
#zle-line-init() {
#    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
#    echo -ne "\e[5 q"
#}
#zle -N zle-line-init
#echo -ne '\e[5 q' # Use beam shape cursor on startup.
#preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
######### end cursor shape block

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
export EDITOR=nvim

# set terminal name to current command
function preexec {
    print -Pn "\e]0;[TERM] ${(q)1}\e\\"
}
# reset terminal name to user@host:cwd
function precmd {
    printf "\033]0;[TERM] %s@%s\007" "${USER}" "${PWD/#$HOME/~}"
}

[ -f "/home/$USER/TODO.txt" ] && cat ~/TODO.txt

# manpager (dosent like being in my profile)
#export MANPAGER="sh -c 'col -bx | bat -fl man -p'"
man() {
    /usr/bin/man "$@" | /usr/share/nvim/runtime/macros/less.sh -c "set ft=man"
}

# replace $PS1 with starship
eval "$(starship init zsh)"
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
#source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# syntax highlighting NOTE MUST BE LAST
source $HOME/.local/share/zsh/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
