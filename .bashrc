#!/bin/bash
# ~/.bashrc
#
~/.profile

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\[$(tput bold)\]\[\033[38;5;92m\][\[$(tput sgr0)\]\[\033[38;5;4m\]\u@\[$(tput sgr0)\]\[\033[38;5;2m\]\w\[$(tput sgr0)\]\[\033[38;5;92m\]]\$\[$(tput sgr0) '
eval "$(starship init bash)"
# sourcing my functions and aliases
source "$HOME"/.config/shell/funcrc.bash
source "$HOME"/.config/shell/aliasrc.bash
export HISTFILE=~/.cache/bash_history

# set vi mode and ctrl+l clears screen
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'
bind 'set enable-bracketed-paste on'

# vicd see functionrc for what it does
bind '"\C-o":"\C-u vicd\n"'

# set a few options
shopt -s expand_aliases
shopt -s cdspell
bind "set completion-ignore-case on"

