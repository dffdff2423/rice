;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; defult notes
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


(load! "snip.el")

;; set name needed for some things
(setq user-full-name "dffdff2423"
      user-mail-address "dffdff2423@gmail.com")

;;; font and theme
;; this mess sets the font and theme
;; (setq doom-theme 'doom-challenger-deep)
(setq doom-theme 'doom-moonlight)

(setq doom-font (font-spec :family "Jet Brains Mono" :size 12 :weight 'light))
;; (setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 10))

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(setq doom-localleader-key ",")

(map! "C-h" #'evil-window-left
      "C-j" #'evil-window-down
      "C-k" #'evil-winodw-up
      "C-l" #'evil-window-right)
;; set transparency
(set-frame-parameter (selected-frame) 'alpha '(95 . 95))
(add-to-list 'default-frame-alist '(alpha . (95 . 95)))

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(95 . 95) '(100 . 100)))))
(map! :leader :desc "toggle transparency" "t t" #'toggle-transparency)
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))


;; set org to my org dir
(setq org-directory "~/doc/org/")

;; Set line numbers remove `relitive' to set to normal line numbers and `nil' to dissable
(setq display-line-numbers-type `relative)

(setq-default fill-column 80)
(global-display-fill-column-indicator-mode +1)

(after! company
  (setq company-minimum-prefix-length 2)
  (setq company-idle-delay 0.2))

(setq lsp-signature-render-documentation nil)

;;; rust
;; NOTE for some reson archlinux's binary screws up everything
;; REMOVE IT and build from source
(after! lsp-rust
  (setq lsp-rust-server 'rust-analyzer)
  (setq lsp-rust-analyzer-server-command '("~/.cargo/bin/rust-analyzer")))
  ;;(setq lsp-rust-analyzer-server-command '("~/.local/bin/rust-analyzer-linux")))
  ;;(setq lsp-rust-analyzer-server-command '("/usr/bin/rust-analyzer")))
(after! rustic
  (setq rustic-lsp-server `rust-analyzer))
;; (after! rustic
;;   (setq rustic-lsp-server `rls))

;; glsl

(after! glsl-mode
  (add-to-list 'auto-mode-alist '("\\.vs\\'" . glsl-mode))
  (add-to-list 'auto-mode-alist '("\\.fs\\'" . glsl-mode)))

;(use-package! company-glsl
;  :config
;  (when (executable-find "glslangValidator")
;    (add-to-list 'company-backends 'company-glsl)))
;;; cc

;; indentation
;;(setq c-default-style "k&r")
(setq c-default-style "linux")
(setq-hook! '(c-mode-hook c++-mode-hook) indent-tabs-mode t)  ; C/C++

;;(after! 'cc-mode
;;  (add-to-list 'c-mode-common-hook
;;               (lambda () (setq c-syntactic-indentation nil))))
(setq-default tab-width 8)

(setq dap-lldb-debug-program `("lldb-vscode"))

;; (after! ccls NOTE DO NOT use this lsp server it doent seem to work
;;   (setq ccls-initialization-options '(:index (:comments 2)
;;                                       :completion (:detailedLabel t)
;;                                       :compilationDatabaseDirectory "build/"
;;                                       :cache (:directory ".cache/ccls"))
;;         ccls-sem-highlight-method 'overlay)
;;   (setq company-transformers nil company-lsp-async t company-lsp-cache-candidates nil)
;;   (set-lsp-priority! 'ccls 2)
;;   (map! :localleader
;;         :map c++-mode-map
;;         :desc "code-lens" "," #'ccls-code-lens-mode
;;         (:prefix-map ("h" . "hierarchy")
;;          :desc "member" "m" #'ccls-member-hierarchy
;;          :desc "calls" "c" #'ccls-calls-hierarchy
;;          :desc "inheritance" "i" #'ccls-inheritance-hierarchy)))
(setq lsp-clients-clangd-args '("-j=3"
                                "--background-index"
                                "--clang-tidy"
                                "--completion-style=detailed"
                                "--header-insertion=never"))
(after! lsp-clangd (set-lsp-priority! 'clangd 2))

(add-hook 'meson-mode-hook 'company-mode)

;;; zig

(use-package! zig-mode
  :hook ((zig-mode . lsp-deferred))
  :custom (zig-format-on-save nil)
  :config
  (after! lsp-mode
    (add-to-list 'lsp-language-id-configuration '(zig-mode . "zig"))
    (lsp-register-client
      (make-lsp-client
        :new-connection (lsp-stdio-connection "<path to zls>")
        :major-modes '(zig-mode)
        :server-id 'zls))))

;;; map keys
(map! :leader
      (:prefix-map ("r" . "run/reload")
       :desc "reload lsp" "l" #'lsp-restart-workspace
       :desc "projectile-gdb" "r" #'projectile-run-gdb))

;; (map! :leader
;;       :desc "open term HERE" "o t" #'term)
;;; tex
(setq +latex-viewers '(zathura))


;; FIXME hide the * in *sjds*
(after! org-mode-hook
  (setq org-hide-emphasis-markers t))

;;; lisp

;; set REPL
;(setq inferior-lisp-program "sbcl")
;
;; (load (expand-file-name "~/.roswell/helper.el"))
; (setq inferior-lisp-program "ros -Q run")
; (setq sly-complete-symbol-function 'sly-flex-completions)

(after! sly (add-to-list 'sly-contribs 'sly-asdf 'append))

(map! (:localleader
       :map lisp-mode-map
       (:prefix ("e" . "evaluate")
        :desc "Evaluate defun"      "e" #'sly-eval-defun
        :desc "Evaluate last"       "l" #'sly-eval-last-expression)))

;; HACK Semicolen in visualmode runs lispy comment
;;(map! :map lispy-mode-map :v ";" #'lispy-comment)


;; bind some keys for lisp based mode
(map! "C->" #'sp-forward-slurp-sexp
      "C-<" #'sp-forward-barf-sexp
      "C-M-<" #'sp-backward-slurp-sexp
      "C-M->" #'sp-backward-barf-sexp
      "C-l" #'sp-forward-sexp
      "C-h" #'sp-backward-sexp
      "C-(" #'sp-copy-sexp)

;;; scheme

(setq geiser-guile-binary "guile3")
(setq flycheck-guile-executable "guild3")
(setq geiser-chicken-binary "chicken-csi")
(setq flycheck-scheme-chicken-executable "chicken-csc")
(setq geiser-repl-history-filename "~/.cache/geiser/history")

(setq scheme-add-keywords
  '(("#\\w*" . font-lock-constant-face)
    ("\\_<\\w*\\?\\_>" . font-lock-variable-name-face)
    ("\\_<\\w*\\!\\_>" . font-lock-function-name-face)))

(font-lock-add-keywords 'scheme-mode scheme-add-keywords)

;;; eshell

(setq eshell-aliases-file "~/.doom.d/aliasrc.esh")

;;; minimap
(setq minimap-highlight-line nil)

;;; java

(after! java
  (setq
   lsp-java-maven-download-sources t
   lsp-java-content-provider-preferred "fernflower"
   lsp-java-workspace-dir "src/java"
   lsp-java-import-gradle-enabled t))
