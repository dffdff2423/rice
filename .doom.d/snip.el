;;; snip.el -*- lexical-binding: t; -*-

;; HACK this is probly not the best way to do this
(set-file-template! "\\.cpp$" :trigger "__cpp" :mode `c++-mode)
(set-file-template! "\\.cxx$" :trigger "__cpp" :mode `c++-mode)
(set-file-template! "\\.hpp$" :trigger "__hpp" :mode `c++-mode)
(set-file-template! "\\.hxx$" :trigger "__hpp" :mode `c++-mode)
(set-file-template! "\\.ipp$" :trigger "__ipp" :mode `c++-mode)
(set-file-template! "\\.ixx$" :trigger "__ipp" :mode `c++-mode)
(set-file-template! "main\\.cpp$" :trigger "__main\.cpp" :mode `c++-mode)
(set-file-template! "main\\.cxx$" :trigger "__main\.cpp" :mode `c++-mode)
(set-file-template! "\\.h$" :trigger "__h" :mode `c-mode)
(set-file-template! "\\.fs$" :trigger "__shader" :mode `glsl-mode)
(set-file-template! "\\.vs$" :trigger "__shader" :mode `glsl-mode)
(set-file-template! "\\.vert$" :trigger "__shader" :mode `glsl-mode)
(set-file-template! "\\.frag$" :trigger "__shader" :mode `glsl-mode)
