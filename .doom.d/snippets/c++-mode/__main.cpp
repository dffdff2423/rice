# -*- mode: snippet -*-
# group: file templates
# contributor: dffdff
# --

int main(int argc, char *argv[])
{
    $0

    return 0;
}
