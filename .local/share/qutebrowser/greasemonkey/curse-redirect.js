// ==UserScript==
// @name           Curseforge Redirect
// @namespace      qutebrowser scripts
// @match          https://curseforge.com/*
// @match          https://www.curseforge.com/*
// @run-at         document-start
// ==/UserScript==

location.href=location.href.replace("www.curseforge.com","legacy.curseforge.com");
