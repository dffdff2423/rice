// ==UserScript==
// @name         lib.rs
// @version      1.0
// @description  crates.io to lib.rs
// NOmatch        *://*.crates.io/*
// @match        *://crates.io/*
// @grant        none
// @run-at       document-start
// ==/UserScript==

(function () {
	'use strict';
	top.location.hostname = "lib.rs";
})();
