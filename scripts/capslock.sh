#!/bin/sh
# make CapsLock behave like Ctrl:
setxkbmap -option ctrl:nocaps

# make short-pressed Ctrl behave like Escape:
xcape -e 'Control_L=Escape'

# both shifts for capslock
setxkbmap -option 'shift:both_capslock'

# https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration
# also /usr/share/X11/locale/en_US.UTF-8/compose
# aka /usr/share/X11/locale/<locale>/compose
# set compose key to the "right fn" key
setxkbmap -option compose:ralt

# set compose key to the "menu" key
#setxkbmap -option compose:menu
