VIM=nvim

# colors
alias grep='grep --color=auto'

# ls aliases
alias ls='ls --color=auto'
alias l='/bin/ls -F'
alias ls='exa -F --color=always --group-directories-first --icons'
alias ll='exa -alF --color=always --group-directories-first --icons --git'
alias la='exa -aF --color=always --group-directories-first --icons'

# color for ./gradlew
#if [ $SHELL = '/usr/bin/zsh' ]; then
#    alias ./gradlew='./gradlew --console=rich'
#fi

# tell emacs binding to die a firey death
alias \
    info='info --vi-keys'

# safety
alias \
    cp='cp -i'\
    mv='mv -i'\
    rm='rm -I'

# youtube-dl
YOUTUBE_DL=yt-dlp
alias \
    ytd="$YOUTUBE_DL --add-metadata --prefer-free-formats" \
    yta="ytd -x -f bestaudio/best"                      \
    ytt="$YOUTUBE_DL --write-thumbnail --skip-download"  \

# shortcuts
alias \
    cls='clear'     \
    p='pacman'      \
    pp='doas --  pacman'\
    pa='paru'       \
    pas='paru --sudoloop' \
    rgtodo='rg "TODO|FIXME|BUG|todo!"' \
    mkdir='mkdir -p'\
    v="$VIM"        \
    vv="doas -- $VIM" \
    e='emacs'       \
    nm='neomutt'    \
    nmutt='neomutt' \
    ncmp='ncmpcpp'

# emacs
alias etty='emacs -nw'
alias e='devour emacs'
alias ec='emacsclient -nc'
#alias emacs='devour emacs'


# .. go brr
alias \
    ..='cd ..'              \
    ...='cd ../..'          \
    ....='cd ../../..'      \
    .....='cd ../../../..'

# devour
alias \
    zathura='devour zathura' \
    z='zathura'

alias vim="$VIM"
alias vi="$VIM"
alias less='less -r '

# devel
alias \
    cmakedb='cmake -G Ninja -D CMAKE_BUILD_TYPE=Debug' \
    cmakedbr='cmake -G Ninja -D CMAKE_BUILD_TYPE=RelWithDebInfo' \
    cmaker='cmake -G Ninja -D CMAKE_BUILD_TYPE=Release'

alias cargo='RUST_BACKTRACE=1 RUSTFLAGS="$RUSTFLAGS -Zshare-generics=y" cargo +nightly '
alias cargos='RUST_BACKTRACE=1 /usr/bin/cargo +stable'

alias n='ninja'

# alias for my dotfile manaement
alias cfg='/usr/bin/git --git-dir=$HOME/.cfg.git/ --work-tree=$HOME'

# make normal gdb work nicely with rust
alias gdb="PYTHONPATH=\"$PYTHONPATH:$HOME/.rustup/toolchains/$(rustup toolchain list |\
    grep default | cut -d ' ' -f1)/lib/rustlib/etc\" gdb"

# sbcl readline
alias sbcr="sbcl --eval \"(init-readline)\""
