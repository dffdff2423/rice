#!/bin/zsh
# Colorful manpages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# clean my $HOME

export GOPATH="$HOME/.local/share/go"
export GOPROXY=direct
# i wish i could do this:
# export GOPATH="/dev/null"
export RLWRAP_HOME="$XDG_CACHE_HOME"/rlwrap
export GDBHISTFILE="$XDG_CACHE_HOME"/gdb/history
# themeing
export GTK_USE_PORTAL=1
export QT_QPA_PLATFORMTHEME="qt6ct"


export PATH=~/.local/bin:~/.roswell/bin:~/.emacs.d/bin:~/.cargo/bin:~/scripts:$HOME/src/pkg/tex/texlive/2023/bin/x86_64-linux:$PATH
export INFOPATH=$INFOPATH:$HOME/src/pkg/tex/texlive/2021/texmf-dist/doc/info
export MANPATH=$HOME/src/pkg/tex/texlive/2021/texmf-dist/doc/man:$HOME/src/pkg/lisp/scheme-manpages:$HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/share/man:
export TERMINAL=alacritty
export EDITOR=nvim
export PAGER=less
export BROWSER=qutebrowser
export HISTSIZE=3000
export HISTCONTROL=ignoreboth:erasedups

export FZF_DEFAULT_COMMAND='fd --type f'

# wine
export WINEUSERNAME=dffdff
# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# bat
export BAT_STYLE='plain'

# c and cxx
export CMAKE_EXPORT_COMPILE_COMMANDS=true
export MAKEOPTS="$MAKEOPTS -j12"
export CLAZY_CHECKS="level0,level1,level2"
export CLAZY_IGNORE_DIRS="/usr/include"

# scheme
export CHICKEN_INSTALL_REPOSITORY="$HOME/.local/share/chicken/lib"
export CHICKEN_REPOSITORY_PATH="/usr/lib/chicken/11:$CHICKEN_INSTALL_REPOSITORY"
export CHICKEN_DOC_REPOSITORY="$HOME/.local/share/chicken/chicken-doc"
export GERBIL_BUILD_CORES=10
export GERBIL_GSC=gambitc

export RUST_LOG_STYLE="always"

# perl
PATH="$HOME/.local/share/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/.local/share/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/.local/share/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/.local/share/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/.local/share/perl5"; export PERL_MM_OPT;

export JANET_TREE="$HOME/.local/share/janet"

export VAGRANT_DEFAULT_PROVIDER=libvirt

# disable COC log
export NVIM_COC_LOG_FILE=/dev/null
