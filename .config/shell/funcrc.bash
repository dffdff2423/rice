# use fzf to jump diretories

djmp() {
    djmp=$(FZF_DEFAULT_COMMAND="fd -t d" fzf || echo '.')
    cd $djmp
    djmp=''
}

# "lld like" command for .exe files

winldd() {
    x86_64-w64-mingw32-objdump "$1" -p | grep 'DLL Name'
}

# ffmpeg convert

ffogg() {
    ffmpeg -i $1 -acodec libvorbis $2
}

ffopus() {
    ffmpeg -i $1 -acodec libopus $2
}

# Extract a diffternt archives
ex () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf -v $1;;
      *.tar.gz)    tar xzf -v $1;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf  -v $1;;
      *.tbz2)      tar xjf -v $1;;
      *.tgz)       tar xzf -v $1;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf -v $1 ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Makes a bunch of files with $1-$CURRENTFILE nameing
mkfiles () {
    for x in {1..10}
    do
        touch "$1-$x"
    done
}

# Open vifm then cd to the output
vicd () {
    tmp="$(mktemp)"
    vifm . --choose-dir "$tmp"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp" >/dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
