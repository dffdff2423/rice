#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run /usr/bin/lxqt-policykit-agent &
#run picom &
#run optimus-manager-qt &
#run lxqt-powermanagement &
#run ckb-next -b &
run unclutter --timeout 10 &
run cbatticon &
run redshift-gtk &
#run dxhd &
#~/.screenlayout/layout.sh &
xinput --set-prop 12 351 0
xrdb -merge ~/.Xresources &
~/scripts/capslock.sh

# caps lock

# make CapsLock behave like Ctrl:
#setxkbmap -option ctrl:nocaps &
#
## make short-pressed Ctrl behave like Escape:
#xcape -e 'Control_L=Escape' &
#
## both shifts for capslock
#setxkbmap -option 'shift:both_capslock' &
#
## set compose key to the "right fn" key
#setxkbmap -option compose:rwin &
#
## set compose key to the "menu" key
##setxkbmap -option compose:menu
