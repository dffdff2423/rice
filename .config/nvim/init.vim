let mapleader = " "
let maplocalleader = ","

"""VIM PLUG"""

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

" lang
Plug 'elixir-editors/vim-elixir' " elixir
Plug 'https://tildegit.org/sloum/gemini-vim-syntax' " gemtext
Plug 'tikhomirov/vim-glsl' " glsl
" Plug 'vlime/vlime', {'rtp': 'vim/'} " common lisp
Plug 'kovisoft/slimv'
Plug 'tbastos/vim-lua'     " lua
Plug 'pest-parser/pest.vim' " pest grammar
Plug 'ron-rs/ron.vim'  " ron
Plug 'cespare/vim-toml' " toml
Plug 'ziglang/zig.vim' " zig
Plug 'perillo/qbe.vim' " qbe
Plug 'Shirk/vim-gas' " gnu as
Plug 'vimwiki/vimwiki'
Plug 'bakpakin/janet.vim'
"Plug 'Olical/conjure' " lisp like languages
Plug 'mfussenegger/nvim-jdtls'
Plug 'LhKipp/nvim-nu'

" misc
Plug 'jiangmiao/auto-pairs'
Plug 'nxvu699134/vn-night.nvim'
Plug 'editorconfig/editorconfig-vim'
Plug 'eraserhd/parinfer-rust', {'do': 'cargo build --release'}
Plug 'tpope/vim-abolish'
Plug 'junegunn/vim-easy-align'
" NOTE: might need junegunn/fzf as a dep
Plug 'junegunn/fzf.vim'
Plug 'p00f/nvim-ts-rainbow'

" lsp
Plug 'neovim/nvim-lspconfig'
Plug 'simrat39/rust-tools.nvim'

Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'hrsh7th/cmp-path'
"Plug 'hrsh7th/cmp-buffer'
"Plug 'PaterJason/cmp-conjure.git'
"Plug 'https://github.com/PaterJason/cmp-conjure.git'
" Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-omni'
Plug 'mfussenegger/nvim-dap'

Plug 'dcampos/nvim-snippy'
Plug 'dcampos/cmp-snippy'

" fancy highlighting
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate', 'branch' : 'master'}
Plug 'nvim-treesitter/playground'
Plug 'theHamsta/nvim-treesitter-commonlisp'

call plug#end()


""""""""""""""

" plugin config
if has('nvim') || has('termguicolors')
  set termguicolors
endif

colorscheme vn-night
set bg=light
" transparent background
hi Normal guibg=NONE ctermbg=NONE
hi LineNr guibg=NONE ctermbg=NONE
hi CursorLineNr guibg=NONE ctermbg=NONE


set hidden

set completeopt=menu,menuone,noselect

lua <<EOF
    require('snippy').setup({
        mappings = {
            is = {
                ['<Tab>'] = 'expand_or_advance',
                ['<S-Tab>'] = 'previous',
            },
            nx = {
                ['<leader>x'] = 'cut_text',
            },
        },
    })

    -- Setup nvim-cmp.
    local cmp = require('cmp')

    cmp.setup({
        snippet = {
            expand = function(args)
              require('snippy').expand_snippet(args.body)
            end,
        },
        mapping = {
            ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
            ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
            ['<C-j>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 'c' }),
            ['<C-k>'] = cmp.mapping(cmp.mapping.select_prev_item(),{ 'i', 'c' }),
            ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
            ['<C-e>'] = cmp.mapping({
                i = cmp.mapping.abort(),
                c = cmp.mapping.close(),
            }),
            ['<CR>'] = cmp.mapping.confirm({ select = true }),
        },
        sources = {
        --cmp.config.sources({
            -- { name = 'conjure' },
            { name = 'nvim_lsp' },
            { name = 'nvim_lsp_signature_help' },
            { name = 'path' },
            { name = 'buffer' },
        },
        --}),
        view = {
            entries = 'native'
        },
    })

    cmp.setup.filetype({ 'lisp' }, {
        sources = {
            { name = "omni" },
            { name = "buffer" },
        },
        completion = {
            autocomplete = false,
        }
    })

    -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
    -- cmp.setup.cmdline('/', {
    --     sources = {
    --         { name = 'buffer' }
    --     }
    -- })

    -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
    -- cmp.setup.cmdline(':', {
    --     sources = cmp.config.sources({
    --         { name = 'path' }
    --     }, {
    --         { name = 'cmdline' }
    --     })
    -- })

    -- Setup lspconfig.
    local capabilities = require('cmp_nvim_lsp').default_capabilities()

    local lspconfig = require('lspconfig')

    local on_attach = function(client, bufnr)
        local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
        local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

        -- Enable completion triggered by <c-x><c-o>
        buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

        -- Mappings.
        local opts = { noremap=true, silent=true }

        -- See `:help vim.lsp.*` for documentation on any of the below functions
        buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
        buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
        buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
        buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
        buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
        buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
        buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
        buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
        buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
        buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
        buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
        buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
        buf_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
        buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
        buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
        buf_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
        buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

    end

    lspconfig.clangd.setup {
        on_attach = on_attach,
        capabilities = capabilities,
        cmd = { "clangd",
                         "-j=6",
                         "--background-index",
                         "--clang-tidy",
                         "--completion-style=detailed",
                         "--header-insertion=never",
                }
    }

    -- lspconfig.rust_analyzer.setup {
    --     on_attach = on_attach,
    --     capabilities = capabilities,
    --     cmd = { "rust-analyzer" },
    -- }

    -- lspconfig.kotlin_language_server.setup {
    --     on_attach = on_attach,
    --     capabilities = capabilities,
    --     cmd = { "~/temp/server/bin/kotlin-language-server" },
    -- }

    local rust_tools = require('rust-tools')
    rust_tools.setup({
        server = {
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                ["rust-analyzer"] = {
                    procMacro = {
                        enable = true
                    },
                    completion = { autoimport = { enable = false } }
                }
            }
        }
    })
    rust_tools.inlay_hints.enable()

    lspconfig.zls.setup {
        on_attach = on_attach,
        capabilities = capabilities,
    }

    lspconfig.gdscript.setup {
        on_attach = on_attach,
        capabilities = capabilities,
    }

    lspconfig.nushell.setup {
        on_attach = on_attach,
        capabilities = capabilities,
    }

    local dap = require("dap")
    dap.adapters.gdb = {
      type = "executable",
      command = "gdb",
      args = { "-i", "dap" }
    }
    dap.configurations.c = {
      {
        name = "Launch",
        type = "gdb",
        request = "launch",
        program = function()
          return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = "${workspaceFolder}",
        stopAtBeginningOfMainSubprogram = false,
      },
    }
    dap.configurations.cpp = dap.configurations.c

    dap.adapters.godot = {
      type = "server",
      host = '127.0.0.1',
      port = 6006,
    }

    dap.configurations.gdscript = {
      {
        type = "godot",
        request = "launch",
        name = "Launch scene",
        project = "${workspaceFolder}",
        port = 6007
      }
    }
EOF


let g:vlime_window_settings = {
            \ "repl": {
                \ "pos": "botright",
                \ "vertical": v:true
            \ }
        \ }
let g:vlime_cl_use_terminal = v:true
let g:vlime_compiler_policy = { "DEBUG":  3 }

let g:slimv_repl_split = 4
let g:slimv_repl_simple_eval = 0
let g:slimv_impl = 'sbcl'
let g:slimv_perferred = 'sbcl'
let g:slimv_keybindings = 2
let g:slimv_package = 1

let g:conjure#client#scheme#stdio#command = "chibi-scheme"
let g:conjure#client#scheme#stdio#prompt_pattern = "> "

autocmd! BufNewFile,BufRead *.vs,*.fs set ft=glsl

let g:vimwiki_list = [{'path' : '~/doc/wiki'}]

"set bg=light
set go=a
set mouse=a

" highlight searches
augroup vimrc-incsearch-highlight
  autocmd!
  autocmd CmdlineEnter /,\? :set hlsearch
  autocmd CmdlineLeave /,\? :set nohlsearch
augroup END

set clipboard=unnamedplus
set autoindent
set fileformat=unix
set nobackup

" tabbing
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
"autocmd BufNewFile,BufRead *.c,*.cc,*.cxx,*.cpp,*.h,*.hh,*.hxx,*.hpp,*.go,*.vs,*.fs,*.vert,*.frag setlocal noexpandtab
autocmd FileType c,cpp,java,glsl,go,asm,nasm,gdscript setlocal noexpandtab softtabstop=0 shiftwidth=0
autocmd FileType lisp,scheme,sexplisp setlocal shiftwidth=2 softtabstop=2
"autocmd FileType sexplisp ParinferOn
autocmd BufNewFile,BufRead *.s,*.S set ft=gas

autocmd FileType lisp,scheme let b:AutoPairs = {
            \ '"':'"',
            \ }

autocmd FileType rust let b:AutoPairs = {
            \ '(':')',
            \ '[':']',
            \ '{':'}',
            \ '"':'"',
            \ }

let g:FactorRoot = "~/src/misc/factor/factor-0.98"

set statusline=
set statusline+=\ %f
set statusline+=%#PmenuSel#
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ [%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ [%l:%c]

" treesitter
lua <<EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = { "cpp", "c", "rust", "lua" }, -- one of "all", "maintained", or a list of languages
    ignore_install = { "javascript" }, -- List of parsers to ignore installing,
    highlight = {
        enable = true,
        -- disable = { "cpp", },    -- list of language that will be disabled
        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
        custom_captures = {
            -- Highlight the @foo.bar capture group with the "Identifier" highlight group.
        --    ["foo.bar"] = "Identifier",
        },
    },
    rainbow = {
        enable = true,
        extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
        --max_file_lines = nil,
    },
    playground = {
        enable = true,
        disable = {},
        updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
        persist_queries = false, -- Whether the query persists across vim sessions
        keybindings = {
            toggle_query_editor = 'o',
            toggle_hl_groups = 'i',
            toggle_injected_languages = 't',
            toggle_anonymous_nodes = 'a',
            toggle_language_display = 'I',
            focus_language = 'f',
            unfocus_language = 'F',
            update = 'R',
            goto_node = '<cr>',
            show_help = '?',
        },
    },
}
EOF

hi link TSConditional TSKeyword
hi TSNamespace guifg=Purple
hi @variable guifg=#DADBEC " standard text color for vn-night

filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

nnoremap c "_c
set nocompatible
"filetype plugin on
syntax on
set encoding=utf-8
set nu rnu
" enable autocompletion:
set wildmode=longest,list,full
" disables automatic commenting on newline:
autocmd filetype * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" splits open at the bottom and right
set splitbelow splitright

" shortcutting split navigation, saving a keypress:
map <c-h> <c-w>h
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l

" replace all is aliased to s.
nnoremap s :%s//g<left><left>

" copy selected text to system clipboard (requires gvim/nvim/vim-x11 installed):
vnoremap <c-c> "+y
map <c-p> "+p
"paste from system clipboard with ctrl+i instead of shift insert
map <s-insert> <c-i>

"automatically deletes all trailing whitespace on save.
autocmd bufwritepre * %s/ \+$//e

"auto update things
autocmd bufwritepost *xresources,*xdefaults !xrdb %
autocmd bufwritepost *dxhd.sh !dxhd -r

" force comments to look ok
autocmd bufread * hi Comment guifg=#3b91bf

" neomutt and markdown stuff
autocmd bufnewfile,bufread *neomutt-*,*.md setlocal spell spelllang=en_us
"map <leader>ts :setlocal spell spelllang=en_us<enter>
autocmd FileType markdown,c,cpp,rust 2mat ErrorMsg '\%81v.'
autocmd bufnewfile,bufread *neomutt* 2mat ErrorMsg '\%77v.'

" FORTH 200x locals
function! OnForthLoad()
    syn region forthStandLocals start='{:\s' end='\s:}' end="$"
    syn match forthStandLocals "{: :}"
    hi link forthStandLocals Type
endfunction

autocmd FileType forth call OnForthLoad()

" impove glsl highlighting
function! OnGlslLoad()
    syn keyword glslQualifier set
    syn keyword glslQualifier push_constant
    syn keyword glslQualifier constants
    syn region  glslPreProc   start="^\s*#\s*\(include\)" skip="\\$" end="$" keepend
    syn keyword glslBuiltinVariable gl_BaseInstance
endfunction

autocmd FileType glsl call OnGlslLoad()

" highlight
"hi Keyword guifg=#eb4129 gui=italic cterm=italic
"                brown is actually red
"hi link Structure Keyword
"hi link cppModifier Keyword
"hi link StorageClass Keyword
hi Keyword guifg=Brown gui=italic cterm=italic
hi Conditional guifg=Brown
hi Statement gui=italic cterm=italic
hi Keyword gui=italic cterm=italic
hi Todo gui=italic cterm=italic
hi Comment gui=italic cterm=italic
set listchars=tab:\|\ " this comment is needed to keep the space
set list

" toggle spelling
map <leader>ts :setlocal spell! spelllang=en_us<CR>

" Show syntax highlighting groups for word under cursor
map <leader><F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>


" hack to force omnicompletion to work
" function! AutoOpenCompletion()
"     if !pumvisible() && ((v:char >= 'a' && v:char <= 'z') || (v:char >= 'A' && v:char <= 'Z'))
"         call feedkeys("\<C-x>\<C-o>", "n")
"     endif
" endfunction
"
" function! ForceOmniCompletion()
" lua << EOF
"     local cmp = require('cmp')
"     -- cmp.sources =
" EOF
"     autocmd InsertCharPre * call AutoOpenCompletion()
" endfunction
"
" "autocmd FileType lisp call ForceOmniCompletion()

nnoremap <silent> <F5> <Cmd>lua require'dap'.continue()<CR>
nnoremap <silent> <F10> <Cmd>lua require'dap'.step_over()<CR>
nnoremap <silent> <F11> <Cmd>lua require'dap'.step_into()<CR>
nnoremap <silent> <F12> <Cmd>lua require'dap'.step_out()<CR>
nnoremap <silent> <Leader>b <Cmd>lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <Leader>B <Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nnoremap <silent> <Leader>lp <Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nnoremap <silent> <Leader>dr <Cmd>lua require'dap'.repl.open()<CR>
nnoremap <silent> <Leader>dl <Cmd>lua require'dap'.run_last()<CR>
