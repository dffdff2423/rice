# dffdff's config
my dotfiles

to deploy run and echo this to your current .?rc:
```sh
alias cfg='/usr/bin/git --git-dir=$HOME/.cfg.git/ --work-tree=$HOME'
```

then run:
```sh
echo ".cfg.git" >> .gitignore
git clone --bare https://gitlab.com/dffdff2423/rice.git $HOME/.cfg
alias cfg='/usr/bin/git --git-dir=$HOME/.cfg.git --work-tree=$HOME'
cfg checkout
```
if it fails with a message like:
```
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .gitignore
Please move or remove them before you can switch branches.
Aborting
```
then run this cool comand I fond on the internet:
```sh
mkdir -p .config-backup && \
cfg checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
cfg checkout
```
dont forget to dissable showing untracked files and init the submodules:
```sh
cfg config --local status.showUntrackedFiles no
cfg submodule init
```

# disclaimer

this works on my computer

assume everything that was not copied from some ware else is licensed under the GPL-3.0-or-later license unless stated otherwise
