#!/bin/sh
term=alacritty
EDITOR=nvim
lwww=qutebrowser
www=chromium

# mod4 + shift + e
$term -e sh ~/scripts/ecfg -e

# mod4 + shift + d
$term -e sh ~/scripts/edoc -e

# mod4 + w
$lwww

# mod4 + shift + w
$www

# mod4 + shift + g
$term -e sh -c neomutt

# mod4 + shift + alt + g
notify-send MAIL 'syncing..'
mw -Y
notify-send MAIL 'done syncing mail'

# mod4 + shift + t
xdotool click --repeat 100 --delay 18 1
