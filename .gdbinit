!export PYTHONPATH=$PYTHONPATH:$HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/etc
add-auto-load-safe-path ~/.rustup/toolchains
dir ~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/etc
set disassembly-flavor intel
set debuginfod enabled off
