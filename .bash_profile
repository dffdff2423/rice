#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Colorful manpages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

#exports
#export PATH=$PATH:/home/andre/.local/bin:~/scripts:~/.emacs.d/bin
export EDITOR=nvim
export PAGER=less
export MANPAGER=less
export HISTSIZE=3000
export HISTCONTROL=ignoreboth:erasedups

# wine
export WINEUSERNAME=dffdff
# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

source ~/.config/shell/profile
